package Data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import Model.Contact;

import Utilities.Utility;

public class DatabaseHandler extends SQLiteOpenHelper {

    public DatabaseHandler(@Nullable Context context) {
        super(context, Utility.DATABASE_NAME,null,Utility.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACT_TABLE = "CREATE TABLE " + Utility.TABLE_NAME + "("
                + Utility.KEY_ID + " INTEGER PRIMARY KEY," + Utility.KEY_NAME + " TEXT,"
                + Utility.KEY_PHONE_NUMBER + " TEXT," + Utility.KEY_CITY + " TEXT,"
                + Utility.KEY_STATE + " TEXT" + ")";

        db.execSQL(CREATE_CONTACT_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + Utility.TABLE_NAME);
        onCreate(db);
    }

    public void addContact(Contact contact){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues value = new ContentValues();
        value.put(Utility.KEY_NAME, contact.getName());
        value.put(Utility.KEY_PHONE_NUMBER, contact.getPhoneNumber());
        value.put(Utility.KEY_CITY, contact.getCity());
        value.put(Utility.KEY_STATE, contact.getState());

        db.insert(Utility.TABLE_NAME, null, value);
        db.close();
    }

    public Contact getContact(int id){
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(Utility.TABLE_NAME, new String[]{Utility.KEY_ID,
        Utility.KEY_NAME, Utility.KEY_PHONE_NUMBER,Utility.KEY_CITY, Utility.KEY_STATE}, Utility.KEY_ID + "=?",
                new String[] {String.valueOf(id)},null, null, null, null);

        if(cursor != null)
            cursor.moveToFirst();

        Contact contact = new Contact(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1),cursor.getString(2),
                cursor.getString(3),cursor.getString(4));

        return contact;
    }

    public List<Contact> getAllContacts() {
        SQLiteDatabase db = this.getReadableDatabase();

        List<Contact> contactList = new ArrayList<>();


        String selectTheHouse = "SELECT * FROM " + Utility.TABLE_NAME;
        Cursor cursor = db.rawQuery(selectTheHouse, null);

        if(cursor.moveToFirst()){
            do{
                Contact contact = new Contact();
                contact.setId(Integer.parseInt(cursor.getString(0)));
                contact.setName(cursor.getString(1));
                contact.setPhoneNumber(cursor.getString(2));
                contact.setCity(cursor.getString(3));
                contact.setState(cursor.getString(4));

                contactList.add(contact);

            }while (cursor.moveToNext());
        }

        return contactList;
    }
}
