package Model;

public class Contact {

    private int id;
    private String name;
    private String phoneNumber;
    private String city;
    private String state;

    public Contact() {
    }

    public Contact(int id, String name, String phoneNumber, String city, String state) {
        this.id = id;
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.city = city;
        this.state = state;
    }

    public Contact(String name, String phoneNumber, String city, String state) {
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.city = city;
        this.state = state;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
