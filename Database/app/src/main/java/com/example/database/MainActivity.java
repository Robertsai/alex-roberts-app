package com.example.database;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.util.List;

import Data.DatabaseHandler;
import Model.Contact;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        DatabaseHandler db = new DatabaseHandler(this);

        Log.d("Insert: ", "Inserting...");
        db.addContact(new Contact("AB", "4128448484", "Pittsburgh","PA"));
        db.addContact(new Contact("Crosby", "4120878787", "Pittsburgh","PA"));
        db.addContact(new Contact("Josh Bell", "4120550505", "Pittsburgh","PA"));
        db.addContact(new Contact("Kevin Love", "4400000000", "Cleveland","OH"));


        Log.d("Reading: ", "Reading all contacts...");
        List<Contact> contactList = db.getAllContacts();

        for(Contact c : contactList){
            String log = "ID: " + c.getId() + " , Name: " + c.getName() + " , Phone: " + c.getPhoneNumber()
                    + " , City: " + c.getCity() + " , State: " + c.getState();
            Log.d("Name: ", log);
        }
    }
}
