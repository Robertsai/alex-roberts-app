package com.example.myactivities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class FirstActivity extends AppCompatActivity {

    private Button SecondPageButton;
    private TextView displayMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        SecondPageButton = (Button) findViewById(R.id.BtnSubmit);

        SecondPageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FirstActivity.this, SecondActivity.class);
                intent.putExtra("Message", "Messages");
                intent.putExtra("SecondMsg", "Hola From First Act");
                intent.putExtra("value", 90210);

                startActivity(intent);


            }
        });

        Bundle myExtras = getIntent().getExtras();

        displayMessage = (TextView) findViewById(R.id.textViewFirstId);

        if (myExtras != null) {
            String myMessage = myExtras.getString("SecondMsg");
            int MyInt= myExtras.getInt("value");
            displayMessage.setText("Message received from Act 2 " +myMessage + " and the number " + String.valueOf(MyInt));
        }
    }


    @Override
    protected void onStart(){
        super.onStart();
        Toast.makeText(FirstActivity.this, "Welcome to Act 1!", Toast.LENGTH_SHORT).show();
    }
}
