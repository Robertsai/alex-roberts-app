package com.example.myactivities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class SecondActivity extends AppCompatActivity {

    private TextView displayMessage;
    private Button firstPageButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        firstPageButton = (Button) findViewById(R.id.BtnSubmit2);

        firstPageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SecondActivity.this, FirstActivity.class);
                intent.putExtra("SecondMsg", "Well Hello Act 1");
                intent.putExtra("value", 10);

                startActivity(intent);


            }
        });

        Bundle myExtras = getIntent().getExtras();

        displayMessage = (TextView) findViewById(R.id.txtViewSecondId);

        if (myExtras != null) {
            String myMessage = myExtras.getString("SecondMsg");
            int MyInt= myExtras.getInt("value");
            displayMessage.setText("Message received from Act 1 " +myMessage + " and the number " + String.valueOf(MyInt));
        }
    }
    @Override
    protected void onStart(){
        super.onStart();
        Toast.makeText(SecondActivity.this, "Act 2 is better!", Toast.LENGTH_SHORT).show();
    }
}