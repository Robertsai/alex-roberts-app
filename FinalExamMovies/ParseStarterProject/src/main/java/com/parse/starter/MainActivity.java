/*
 * Copyright (c) 2015-present, Parse, LLC.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */
package com.parse.starter;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.GetCallback;
import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseAnalytics;
import com.parse.ParseAnonymousUtils;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;


public class MainActivity extends AppCompatActivity {

    private Button Next;
    private Button Home;
    private EditText MovieName;
    private EditText Rating;
    private EditText Genre;
    private EditText Star;
    private EditText Reviews;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    Next = (Button) findViewById(R.id.btnNext);
    Home = (Button) findViewById(R.id.btnHome);

    MovieName = (EditText) findViewById(R.id.txtTitle);
    Rating = (EditText) findViewById(R.id.txtRating);
    Genre = (EditText) findViewById(R.id.txtGenre);
    Star = (EditText) findViewById(R.id.txtStar);
    Reviews = (EditText) findViewById(R.id.txtReview);

    Next.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    ParseObject movie = new ParseObject("Movie");
                                    movie.put("MovieName", MovieName.getText().toString());
                                    movie.put("Rating", Rating.getText().toString());
                                    movie.put("Genre", Genre.getText().toString());
                                    movie.put("Star", Star.getText().toString());
                                    movie.put("Reviews", Reviews.getText().toString());
                                    movie.saveInBackground(new SaveCallback() {
                                        @Override
                                        public void done(ParseException e) {
                                            if (e == null) {
                                                Log.i("Success", "We successfully added your movie!");
                                                Toast.makeText(MainActivity.this, "We successfully added your movie!", Toast.LENGTH_SHORT).show();
                                            } else {
                                                e.printStackTrace();
                                            }
                                        }
                                    });
                                }
                            });

      Home.setOnClickListener(new View.OnClickListener() {
                                  @Override
                                  public void onClick(View v) {

                                      MovieName.setText("Movie Title");
                                      Rating.setText("Rating");
                                      Genre.setText("Genre");
                                      Star.setText("Star of the film");
                                      Reviews.setText("Rotten Tomatoes Rating");
                                      Next.setVisibility(View.INVISIBLE);
                                  }

                              });

/*
    ParseQuery<ParseObject> query = ParseQuery.getQuery("Stuff");
    query.getInBackground("rtwlx31WkM", new GetCallback<ParseObject>() {
      @Override
      public void done(ParseObject object, ParseException e) {
        if(e == null && object != null){

            object.put("location","Sam");
            object.saveInBackground();


            object.put("object","green eggs and ham");
            object.saveInBackground();


          Log.i("object", object.getString("object"));
          Log.i("location", object.getString("location"));
        }
      }
    });
*/
    ParseAnalytics.trackAppOpenedInBackground(getIntent());
  }
}