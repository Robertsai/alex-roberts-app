package com.example.myapplication;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private MediaPlayer mediaPlayer;
    private Button playButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mediaPlayer = new MediaPlayer();
        mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.suspense);

        int duration = mediaPlayer.getDuration();
        String mDuration = String.valueOf(duration);

        Toast.makeText(getApplicationContext(), "Duration: "+
                mDuration, Toast.LENGTH_LONG).show();

        playButton = (Button) findViewById(R.id.playID);
        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mediaPlayer.isPlaying()) {
                    pauseMusic();
                } else {
                    startMusic();
                }
            }
        });
    }
    public void pauseMusic(){
        if(mediaPlayer !=null){
            mediaPlayer.pause();
            playButton.setText("Play");

            int currentPosition = mediaPlayer.getCurrentPosition();
            String mCurrentPosition = String.valueOf(currentPosition);

            int duration = mediaPlayer.getDuration();
            String mDuration = String.valueOf(duration);

            Toast.makeText(getApplicationContext()," Duration: " + mDuration + " Current Position: "+
                    mCurrentPosition, Toast.LENGTH_LONG).show();
        }
    }

    public void startMusic(){
        if(mediaPlayer != null){
            mediaPlayer.start();
            playButton.setText("Pause");

            int currentPosition = mediaPlayer.getCurrentPosition();
            String mCurrentPosition = String.valueOf(currentPosition);

            int duration = mediaPlayer.getDuration();
            String mDuration = String.valueOf(duration);

            Toast.makeText(getApplicationContext()," Duration: " + mDuration + " Current Position: "+
                    mCurrentPosition, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onDestroy(){
        if(mediaPlayer != null && mediaPlayer.isPlaying()){
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
        }
        super.onDestroy();
    }

}
