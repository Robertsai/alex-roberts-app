package com.example.eviltestfinal;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class Good extends AppCompatActivity {
    private AnimationDrawable goodAnimation;
    private ImageView goodImage;
    private MediaPlayer goodPlayer;
    private Button restart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_good);
        Handler myHandler = new Handler();
        myHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Animation startanimation = AnimationUtils.loadAnimation(getApplicationContext(),
                        R.anim.fade_in_animation);
                goodImage.startAnimation(startanimation);
            }
        }, 50);

        goodImage = (ImageView) findViewById(R.id.goodID);
        goodImage.setBackgroundResource(R.drawable.good_anim);
        goodAnimation = (AnimationDrawable) goodImage.getBackground();

        goodPlayer = new MediaPlayer();
        goodPlayer = MediaPlayer.create(getApplicationContext(), R.raw.good);

        restart = (Button)findViewById(R.id.btnRestart);

        restart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Good.this, MainActivity.class);

                startActivity(intent);
            }
        });


    }

    public void startMusic(MediaPlayer mp) {
        if (mp != null) {
            mp.start();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        goodAnimation.start();

        startMusic(goodPlayer);

        restart.setVisibility(View.VISIBLE);

        Bundle myextras = getIntent().getExtras();

        TextView hmm = (TextView)findViewById(R.id.hmmtxtView);

        if(myextras != null){
            String myName = myextras.getString("Name");
            hmm.setText("Looks like you're a pretty awesome person, "+myextras.getString("Name")+"!");
        }

        return super.onTouchEvent(event);
    }

}
