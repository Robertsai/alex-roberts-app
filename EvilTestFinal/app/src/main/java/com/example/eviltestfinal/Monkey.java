package com.example.eviltestfinal;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


public class Monkey extends AppCompatActivity {
    private AnimationDrawable monkeyAnimation;
    private ImageView monkeyImage;
    private MediaPlayer monkeyPlayer;
    private Button restart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monkey);

        Handler myHandler = new Handler();
        myHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Animation startanimation = AnimationUtils.loadAnimation(getApplicationContext(),
                        R.anim.fade_in_animation);
                monkeyImage.startAnimation(startanimation);
            }
        }, 50);

        monkeyImage = (ImageView) findViewById(R.id.monkeyID);
        monkeyImage.setBackgroundResource(R.drawable.monkey_anim);
        monkeyAnimation = (AnimationDrawable) monkeyImage.getBackground();

        monkeyPlayer = new MediaPlayer();
        monkeyPlayer = MediaPlayer.create(getApplicationContext(), R.raw.monkey);

        restart = (Button)findViewById(R.id.btnRestart);

        restart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Monkey.this, MainActivity.class);
                startActivity(intent);
            }
        });

    }

    public void startMusic(MediaPlayer mp) {
        if (mp != null) {
            mp.start();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        monkeyAnimation.start();


        startMusic(monkeyPlayer);

        restart.setVisibility(View.VISIBLE);

        Bundle myextras = getIntent().getExtras();

        TextView hmm = (TextView)findViewById(R.id.hmmtxtView);

        if(myextras != null){
            String myName = myextras.getString("Name");
            hmm.setText("Looks like you're just kinda okay, "+myextras.getString("Name")+"...");
        }



        return super.onTouchEvent(event);
    }

}
