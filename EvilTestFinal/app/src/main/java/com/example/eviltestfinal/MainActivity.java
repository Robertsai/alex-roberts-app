package com.example.eviltestfinal;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;


public class MainActivity extends AppCompatActivity {
    private ImageButton GoodButton;
    private ImageButton MonkeyButton;
    private ImageButton EvilButton;
    private Button StartButton;
    private EditText nameText;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        nameText = (EditText)findViewById(R.id.editText);

        GoodButton = (ImageButton) findViewById(R.id.btnGood);

        GoodButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Good.class);

                startActivity(intent);
            }
        });

        MonkeyButton = (ImageButton) findViewById(R.id.btnMonkey);

        MonkeyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Monkey.class);

                startActivity(intent);
            }
        });

        EvilButton = (ImageButton) findViewById(R.id.btnEvil);

        EvilButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Bad.class);
                startActivity(intent);
            }
        });

        StartButton = (Button) findViewById(R.id.btnStart);

        StartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String InputText;
                InputText = nameText.getText().toString();

                Intent intent = new Intent(MainActivity.this, Questions.class);
                intent.putExtra("Name",InputText);


                startActivity(intent);
            }
        });

    }

}
