package com.example.eviltestfinal;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.util.jar.Attributes;

public class Questions extends AppCompatActivity {
    private ToggleButton toggleButton;
    private ToggleButton toggleButton2;
    private ToggleButton toggleButton3;
    private ToggleButton toggleButton4;
    private ToggleButton toggleButton5;
    private ToggleButton toggleButton6;
    private ToggleButton toggleButton7;
    private ToggleButton toggleButton8;
    private ToggleButton toggleButton9;
    private ToggleButton toggleButton10;
    private Integer evilTally;
    private Button endButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questions);

        evilTally = 0;


        Bundle myextras = getIntent().getExtras();

        TextView badge = (TextView)findViewById(R.id.textView);

        if(myextras != null){
            badge.setText(myextras.getString("Name")+" use the buttons to toggle on which thing" +
                    "you prefer. You can press it a few times to get it right.");
        }

        endButton = (Button)findViewById(R.id.btnEnd);
        endButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                toggleButton = (ToggleButton)findViewById(R.id.toggleButton);
                if(toggleButton.isChecked()){
                    evilTally += 1;
                }
                toggleButton2 = (ToggleButton)findViewById(R.id.toggleButton2);
                if(toggleButton2.isChecked()){
                    evilTally += 1;
                }

                toggleButton3 = (ToggleButton)findViewById(R.id.toggleButton3);
                if(toggleButton3.isChecked()){
                    evilTally += 1;
                }

                toggleButton4 = (ToggleButton)findViewById(R.id.toggleButton4);
                if(toggleButton4.isChecked()){
                    evilTally += 1;
                }

                toggleButton5 = (ToggleButton)findViewById(R.id.toggleButton5);
                if(toggleButton5.isChecked()){
                    evilTally += 1;
                }

                toggleButton6 = (ToggleButton)findViewById(R.id.toggleButton6);
                if(toggleButton6.isChecked()){
                    evilTally += 1;
                }

                toggleButton7 = (ToggleButton)findViewById(R.id.toggleButton7);
                if(toggleButton7.isChecked()){
                    evilTally += 1;
                }

                toggleButton8 = (ToggleButton)findViewById(R.id.toggleButton8);
                if(toggleButton8.isChecked()){
                    evilTally += 1;
                }

                toggleButton9 = (ToggleButton)findViewById(R.id.toggleButton9);
                if(toggleButton9.isChecked()){
                    evilTally += 1;
                }

                toggleButton10 = (ToggleButton)findViewById(R.id.toggleButton10);
                if(toggleButton10.isChecked()){
                    evilTally += 1;
                }

                if(evilTally > 4){
                    if(evilTally < 7){
                        Bundle myextras = getIntent().getExtras();

                        Toast.makeText(Questions.this, "Interesting choices...", Toast.LENGTH_SHORT).show();

                        String InputText;
                        InputText = myextras.getString("Name");

                        Intent intent = new Intent(Questions.this, Monkey.class);
                        intent.putExtra("Name",InputText);
                        startActivity(intent);
                        evilTally = 0;
                    }else{
                        Bundle myextras = getIntent().getExtras();

                        Toast.makeText(Questions.this, "Yikes... You may not want to know...", Toast.LENGTH_SHORT).show();

                        String InputText;
                        InputText = myextras.getString("Name");
                        Intent intent = new Intent(Questions.this, Bad.class);
                        intent.putExtra("Name", InputText);
                        startActivity(intent);
                        evilTally = 0;
                    }
                }else{
                    Bundle myextras = getIntent().getExtras();

                    Toast.makeText(Questions.this, "Not bad.", Toast.LENGTH_SHORT).show();

                    String InputText;
                    InputText = myextras.getString("Name");

                    Intent intent = new Intent(Questions.this, Good.class);
                    intent.putExtra("Name",InputText);
                    startActivity(intent);
                    evilTally = 0;
                }

            }
        });


    }
}
