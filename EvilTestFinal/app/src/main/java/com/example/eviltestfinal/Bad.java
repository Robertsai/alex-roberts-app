package com.example.eviltestfinal;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


public class Bad extends AppCompatActivity {
    private AnimationDrawable badAnimation;
    private ImageView badImage;
    private MediaPlayer badPlayer;
    private Button restart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bad);

        Handler myHandler = new Handler();
        myHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Animation startanimation = AnimationUtils.loadAnimation(getApplicationContext(),
                        R.anim.fade_in_animation);
                badImage.startAnimation(startanimation);
            }
        }, 50);

        badImage = (ImageView) findViewById(R.id.badID);
        badImage.setBackgroundResource(R.drawable.evil_anim);
        badAnimation = (AnimationDrawable) badImage.getBackground();

        badPlayer = new MediaPlayer();
        badPlayer = MediaPlayer.create(getApplicationContext(), R.raw.evil);

        restart = (Button)findViewById(R.id.btnRestart);

        restart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Bad.this, MainActivity.class);

                startActivity(intent);
            }
        });

    }

    public void startMusic(MediaPlayer mp) {
        if (mp != null) {
            mp.start();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        badAnimation.start();

        startMusic(badPlayer);

        restart.setVisibility(View.VISIBLE);

        Bundle myextras = getIntent().getExtras();

        TextView hmm = (TextView)findViewById(R.id.hmmtxtView);

        if(myextras != null){
            String myName = myextras.getString("Name");
            hmm.setText("Looks like you're a jerk, "+myextras.getString("Name")+"!");
        }



        return super.onTouchEvent(event);
    }

}
